import datetime

from config import ConfigDirectories
from iss.load import ISS
from structure.directories import Folders
from structure.tickers import Tickers
from data_analysis.aggregator import DataReader


class APP:

    def __init__(self):
        self.Folds = Folders()
        self.Folds.create_default_structure()
        self.Ticks = Tickers()


    def load_full_year(self, years):
        for year in years:
            ISS().run_load_full_year(year, 
                                    eq_tickers = self.Ticks.get_eq_tickers(),
                                    fx_tickers = self.Ticks.get_fx_tickers(), 
                                    netflow_tickers = self.Ticks.get_netflow_tickers())


    def load_last_days(self, number_of_days):
            ISS().run_load_last_days(futoi_tickers = self.Ticks.get_futoi_tickers(),
                                    number_of_days = number_of_days)


    def load(self, years = False, days = False):
        if years:
            self.load_full_year(years)
        
        if days:
            self.load_last_days(days)


    def aggregate(self):        
        DataReader().aggregate_data('si')
        DataReader().aggregate_data('ri')



if __name__ == "__main__":
    
    App = APP()
    App.load(years = [2020, 2021], days = 30)
    App.aggregate()
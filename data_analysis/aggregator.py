import os
import pandas as pd
import datetime
from time import time
from config import ConfigDirectories



class DataReader:

    def __init__(self):
        self.directory = 'futoi'
        self.files = self.return_paths()


    def return_files(self):
        path = os.path.join(os.getcwd(), self.directory)
        return os.listdir(path)


    def return_ticker(self, file: str):
        return file[:-15]


    def return_paths(self):
        path = os.path.join(os.getcwd(), self.directory)
        files = self.return_files()
        
        results = []
        for file in files:
            file_path = os.path.join(path, file)
            ticker = self.return_ticker(file)
            data = (file_path, ticker)
            results.append(data)

        return results


    def normalize_futoi(self, df):
        df['tradedate'] = pd.to_datetime(df['tradedate'], format = '%Y-%m-%d')
        df['tradedate'] = df['tradedate'].dt.date


    def read_data(self, path: str):

        if self.directory == ConfigDirectories().futoi:
            df = pd.read_csv(path, sep = ';', skiprows=1)
            self.normalize_futoi(df)

        return df.iloc[:-3,:]
    

    def concatinate_data(self, ticker):
        
        file = self.files[0]
        df_1 = self.read_data(path = file[0]).iloc[:0,:]

        for file in self.files:
            if file[1] == ticker:
                df = self.read_data(path = file[0])
                df_1 = df_1.append(df)
            
        return df_1


    def split_by_client_type(self, df):
        individual = df[df['clgroup'] == 'FIZ'].sort_values(by=['systime'], ascending = False)
        entity = df[df['clgroup'] == 'YUR'].sort_values(by=['systime'], ascending = False)

        individual.reset_index(inplace = True)
        entity.reset_index(inplace = True)

        del individual['index']
        del entity['index']

        individual['tradetime'] = individual['tradetime'].apply(lambda x: x[:5])
        entity['tradetime'] = entity['tradetime'].apply(lambda x: x[:5])

        return (individual, entity)

    
    def aggregate_data(self,ticker):
        t0 = time()

        df = self.concatinate_data(ticker)
        individual, entity = self.split_by_client_type(df)

        path = os.path.join(os.getcwd(), ConfigDirectories().results)

        file_name_ind = os.path.join(path, '%s-fiz.csv' % ticker)
        file_name_ent = os.path.join(path, '%s-yur.csv' % ticker)

        individual.to_csv(file_name_ind)
        entity.to_csv(file_name_ent)
                
        t1 = time() - t0
        print('Success aggregation - %s: %s sec.\n' % (ticker, round(t1, 2)))

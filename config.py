import configparser
import os


class Configuration:

    def get_data(self, section: str, parameter: str):
        config = configparser.ConfigParser()
        config_file = os.getcwd() + '/config.ini'
        config.read(config_file)
        
        return config[section].get(parameter)


class ConfigDirectories(Configuration):
    def __init__(self):
        super().__init__()
        self.quotes = self.get_data('DIRECTORIES', 'quotes')
        self.netflow = self.get_data('DIRECTORIES', 'netflow')
        self.futoi = self.get_data('DIRECTORIES', 'futoi')
        self.results = self.get_data('DIRECTORIES', 'results')


class ConfigISS(Configuration):
    def __init__(self):
        super().__init__()
        self.delay = self.get_data('ISS', 'delay')
        self.login = self.get_data('ISS', 'login')
        self.password = self.get_data('ISS', 'password')


class ConfigTickers(Configuration):
    def __init__(self):
        super().__init__()
        self.i3 = ['AFLT', 'ALRS', 'CHMF', 'GAZP', 'GMKN', 'LKOH', 'MAGN', 'MGNT', 'MOEX', 
                'ROSN', 'SBER', 'SNGS', 'SNGSP', 'VTBR', 'YNDX']

        self.fx = ['USD000UTSTOM', 'USD000000TOD', 'EUR_RUB__TOM', 'EUR_RUB__TOD']

        self.netflow = ['AFLT', 'ALRS', 'FEES', 'FIVE', 'GAZP', 'GMKN', 'HYDR', 'IRAO', 'LKOH', 
                        'MAGN', 'MGNT', 'MTLR', 'MTSS', 'NLMK', 'NVTK', 'RASP', 'ROSN', 'SBER', 
                        'SBERP', 'SIBN', 'SNGS', 'TATN', 'VTBR', 'YNDX']
        
        self.futoi = ['si', 'ri']

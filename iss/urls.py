import datetime
import os

from config import ConfigDirectories, ConfigISS


class IssUrls:
    def __init__(self, file_format: str, ticker: str, year: str):
        self.file_format = file_format
        self.ticker = ticker
        self.year = year


    def get_date(self):
        today = datetime.datetime.today() 
        today_shift = today - datetime.timedelta(days=int(ConfigISS().delay))

        if str(self.year) == str(today.year):
            day = today_shift.strftime("-%m-%d")
        else:
            day = '-12-31'
        return day


    def get_year_dates(self):
        today = datetime.datetime.today() 
        results = []

        if str(self.year) == str(today.year):
            day = today

            while day.strftime("-%m-%d") != "-01-01":
                results.append(day.strftime("-%m-%d"))
                day = day - datetime.timedelta(1)

        else:
            day = datetime.datetime(year = 2019, month = 12, day = 31)

            while day.strftime("-%m-%d") != "-01-01":
                results.append(day.strftime("-%m-%d"))
                day = day - datetime.timedelta(1)

        return results


    def get_last_days(self, number_of_days):
        day = datetime.datetime.today() 
        results = []

        for item in range(1, number_of_days+1):
            results.append(day.strftime("-%m-%d"))
            day = day - datetime.timedelta(item)

        return results


    def dates_for_candles(self):
        dates = '{ticker}/candles{file_format}'\
                '?from={year}-01-01'\
                '&till={year}-12-31'\
                '&interval=24'
        return dates.format(file_format = self.file_format, 
                            ticker = self.ticker, 
                            year = self.year)


    def dates_for_flow(self):
        dates = '{ticker}{file_format}'\
                '?from={year}-01-01'\
                '&till={year}{day}'
        return dates.format(file_format = self.file_format, 
                            ticker = self.ticker, 
                            year = self.year, 
                            day = self.get_date())


    def dates_for_futoi(self, day):
        dates = '{ticker}{file_format}'\
                '?from={year}{day}'\
                '&till={year}{day}'
        return dates.format(file_format = self.file_format, 
                            ticker = self.ticker, 
                            year = self.year, 
                            day = day)


    def get_eq_url(self):
        url = 'http://iss.moex.com/iss/engines/stock/markets/shares/securities/' 
        return url + self.dates_for_candles()


    def get_fx_url(self):        
        url = 'http://iss.moex.com/iss/engines/currency/markets/selt/boards/cets/securities/' 
        return url + self.dates_for_candles()


    def get_netflow_url(self):
        url = 'http://iss.moex.com/iss/analyticalproducts/netflow2/securities/'
        return url + self.dates_for_flow()


    def get_futoi_url(self, day):
        url = 'https://iss.moex.com/iss/analyticalproducts/futoi/securities/'
        return url + self.dates_for_futoi(day)



class FilesPath:
    def __init__(self, file_format: str, ticker: str, year: str):
        self.file_format = file_format
        self.ticker = ticker
        self.year = year


    def create_file_name(self, day):
        return '%s-%s%s%s' % (self.ticker, self.year, day, self.file_format)


    def create_path_to_save(self, directory, day):
        file_name = self.create_file_name(day)
        return os.path.join(os.getcwd(), directory, file_name)



class UrlFileFactory(IssUrls, FilesPath):
    def __init__(self, file_format: str, ticker: str, year: str, number_of_days: int):
        super().__init__(file_format, ticker, year)  
        self.number_of_days = number_of_days
        self.dates = self.get_dates()
        self.factory = dict()
        self.create()


    def get_url(self, name):
        return self.factory[name]['url']


    def get_path(self, name):
        return self.factory[name]['path']


    def get_dates(self):
        if self.number_of_days:
            return self.get_year_dates()
        else:
            return self.get_last_days(self.number_of_days)


    def create_url_path(self, url_file_name, url, directory, day = ''):
        abspath = self.create_path_to_save(directory, day)
        url_path = dict(url = url, path = abspath)
        self.factory[url_file_name] = url_path


    def create(self): 
        Dir = ConfigDirectories()

        self.create_url_path('eq', self.get_eq_url(), Dir.quotes), 
        self.create_url_path('fx', self.get_fx_url(), Dir.quotes),
        self.create_url_path('netflow', self.get_netflow_url(), Dir.netflow)

        for index, day in enumerate(self.dates):
            self.create_url_path('futoi %s' % str(index), self.get_futoi_url(day), Dir.futoi, day)
import os

from time import time
from typing import Union
import requests

from .urls import UrlFileFactory
from .httploader import AsyncHttpLoader
from config import ConfigISS


class ISS:
    
    def __init__(self):
        self.dates = None
        self.connection = ConfigISS()
        self.cookies = self.get_cookies()
        

    def get_cookies(self):
        login = self.connection.login
        password = self.connection.password
        s = requests.Session()
        s.get('https://passport.moex.com/authenticate', auth=(login, password))

        return {'MicexPassportCert': s.cookies['MicexPassportCert']}


    def get_urls_list(self, tickers, year, url_name, number_of_days):
        urls = []
        paths = []
        
        for ticker in tickers:
            ISS = UrlFileFactory('.csv', ticker, year, number_of_days)
            urls.append(ISS.get_url(url_name))
            paths.append(ISS.get_path(url_name))

        return (urls, paths)


    def load_data(self, tickers, year, url_name, number_of_days = False):
        urls, paths = self.get_urls_list(tickers, year, url_name, number_of_days)
        AsyncHttpLoader(urls = urls, file_abs_paths = paths, cookies = self.cookies).start()


    def run_load_full_year(self, 
                year: str,
                eq_tickers: Union[bool, list] = False, 
                fx_tickers: Union[bool, list] = False,
                netflow_tickers: Union[bool, list] = False,
                futoi_tickers: Union[bool, list] = False):
    
        t0 = time()

        if eq_tickers:
            self.load_data(eq_tickers, year, url_name = 'eq')
        
        if fx_tickers:
            self.load_data(fx_tickers, year, url_name = 'fx')
        
        if netflow_tickers:
            self.load_data(netflow_tickers, year, url_name = 'netflow')
        
        if futoi_tickers:
            for item in range(365):
                try:
                    self.load_data(futoi_tickers, year, url_name = 'futoi %s' % str(item))
                except:
                    pass

        t1 = time() - t0
        print('Success load quotes and flows - %s year: %s sec.\n' % (year, round(t1, 2)))


    def run_load_last_days(self, futoi_tickers, number_of_days):
    
        t0 = time()
        
        for item in range(number_of_days):
            self.load_data(futoi_tickers, 
                            year = 2021, 
                            url_name = 'futoi %s' % str(item), 
                            number_of_days = number_of_days)

        t1 = time() - t0
        print('Success load FUTOI - %s days: %s sec.\n' % (number_of_days, round(t1, 2)))
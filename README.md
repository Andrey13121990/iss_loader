##  Async ISS loader

---

**Конфигурация:**

- [configurations](./config.ini)
- [dependencies](./requirements.txt)

---

**Запуск:**

    python3 run.py


**Виртуальное окружение:**

    python -m venv env
    source env/bin/activate
    pip install -r requirements.txt


---

**Прочие параметры:**

- [Доп. конфигурация](./config.py)